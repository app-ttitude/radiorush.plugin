﻿Bienvenido al plugin de RadioRush por App-ttitude.com

Para instalar este plugin en tu sitio web te recomendamos hacerlo por medio de un subdominio y que de esta manera tengas un directorio dedicado completamente al plugin.

ya que cuentes con un subdominio o directorio dedicado para el plugin procede a copiar todos los archivos existentes en esta carpeta a la raiz de tu subdominio o a el directorio dedicado que creaste para el plugin.

Esto puedes hacerlo descargando un archivo comprimido ZIP desde la sección descargas de mi repositorio en Bitbucket:
https://bitbucket.org/app-ttitude/radiorush.plugin

También puedes desplegar directamente el repositorio en tu sitio web si tu provedor de hosting cuenta con esta función usando los siguientes datos:
URL del repositorio: https://bitbucket.org/app-ttitude/radiorush.plugin.git
Rama: master

Para finalizar simplemente abre con tu editor de textos preferido el archivo config.php.plugin.

Con el archivo abierto rellena los campos que se requieren prestando atención a la clave de licencia y al tipo de licencia ya que estos parámetros son de suma importancia para el funcionamiento correcto del plugin.

Una vez que termines de agregar los datos requeridos, guarda el archivo y renómbralo como config.php (quitando la última parte).

Con esto tu plugin de RadioRush estará listo para usarse accediendo a el subdominio o al directorio que le asignaste.

IMPORTANTE:
* RadioRush está registrado bajo derechos de autor y las licencias no permiten la modificación del código fuente del mismo bajo ninguna circunstancia. De igual manera queda prohibida la realización de ingeniería inversa o manipulación del código con cualquier fin.
En caso de detectar que el plugin fué modificado o alterado App-ttitude.com se reserva el derecho de bloquear la clave de licencia que se use para dicha instalación sin derecho a reembolso del costo de la misma si aplica.
* Asegúrese de que la clave de licencia. el tipo de licencia y la contraseña de API sean correctas basándose en lo siguiente:

1. La clave de licencia debe contener 25 carácteres.
2. El tipo de licencia solo puede ser:
    * branded: para usar el plugin con el pie de página con los derechos de autor y los enlaces al sitio web de App-ttitude.com.
    * nobranded: para usar el plugin solo con el aviso de derechos de distribución de tu empresa o marca.
    * sdk: unicamente cuando hayas adquirido una clave de licencia tipo SDK para desarrollar tu propia interfaz web.
3. Los siguientes campos son obligatorios dentro de la configuración:
    * licence
    * licencetype
    * panelname
    * panelurl
    * companyname
    * companyurl
    * centovaurl
4. los campos opcionales no son obligatorios pero se recomiendan.

Gracias por usar RadioRush.

	/*
		* Este archivo es protegido por la ley del derechos de propiedad intelectual. La copia, reproduccion o decodificacion de este codigo se prohibe estrictamente.
		* RadioRush Plugin está protegido bajo derechos de autor a nivel internacional.
		* Prohibida su modificación total o parcial así como su distribución o reproducción (utilización) de forma total o parcial con fines comerciales.
		* Para mayor información por favor ingrese al enlace de derechos de autor ubicado al pie de cualquier página.
		* Si requiere adquirir una licencia de distribución comercial o para la modificación de este software por favor contacte acualquiera de los siguientes correos electrónicos:
		* pbaltazar@app-ttitude.com
		* sieg.sb@gmail.com
		* Prohibida la realización de ingeniería inversa, crackeado, o cualquier otro método de alteración del funcionamiento normal de este software o acciones que puedan dañar la integridad del código fuente del mismo.
		* RadioRush Panel � 2016, Todos los derechos reservados.
		* RadioRush API � 2016, Todos los derechos reservados.
		* RadioRush Plugin � 2016, Todos los derechos reservados.
		* Centova Cast�, IceCast�, Shoutcast� son marcas registradas independientes a este panel y no guardan relación comercial directa con el mismo.
	*/