﻿<?php @session_start();
	/*
		* Este archivo es protegido por la ley del derechos de propiedad intelectual. La copia, reproduccion o decodificacion de este codigo se prohibe estrictamente.
		* RadioRush Plugin está protegido bajo derechos de autor a nivel internacional.
		* Prohibida su modificación total o parcial así como su distribución o reproducción (utilización) de forma total o parcial con fines comerciales.
		* Para mayor información por favor ingrese al enlace de derechos de autor ubicado al pie de cualquier página.
		* Si requiere adquirir una licencia de distribución comercial o para la modificación de este software por favor contacte acualquiera de los siguientes correos electrónicos:
		* pbaltazar@app-ttitude.com
		* sieg.sb@gmail.com
		* Prohibida la realización de ingeniería inversa, crackeado, o cualquier otro método de alteración del funcionamiento normal de este software o acciones que puedan dañar la integridad del código fuente del mismo.
		* RadioRush � 2016, Todos los derechos reservados.
		* RadioRush API � 2016, Todos los derechos reservados.
		* RadioRush Plugin � 2016, Todos los derechos reservados.
		* Centova Cast�, IceCast�, Shoutcast� son marcas registradas independientes a este panel y no guardan relación comercial directa con el mismo.
	*/
	include_once("../config.php");
	include_once("../sdk/RadioRush.php");
	include_once("tools.php");
	$_SESSION['messagemanualupdate'] = "";
	$_SESSION['messageautoupdate'] = "";
	if(isset($_POST['update'])) {
		$centovauser = $_SESSION['user'];
		$centovapass = $_SESSION['pass'];
		$centovalist = $_POST['list'];
		$centovaaction = $_POST['action'];
		$centovatype = $_GET['type'];
		$server = new RadioRush;
		$server->setConnect($licence, $licencetype, $centovauser, $centovapass);
			if($centovatype == "auto") {
				if($centovaaction == "add") {
					$autoadd = $server->autoAdd($centovalist);
					if($autoadd->status == "OK") {
						$_SESSION['messageautoupdate'] = "La lista de reproducción ".$_POST['list']." ha sido actualizada correctamente. Se agregaron todas las canciones de la carpeta ".$_POST['list'];
						redirect($panelurl, 'playlist');
					} else {
						$_SESSION['messageautoupdate'] = "Error: La lista de reproducción ".$_POST['list']." no pudo ser actualizada. El servidor respondió '".$autoadd->response."'.";
						redirect($panelurl, 'playlist');
					}
				} else {
					$autoremove = $server->autoRemove($centovalist);
					if($autoremove->status == "OK") {
						$_SESSION['messageautoupdate'] = "La lista de reproducción ".$_POST['list']." ha sido actualizada correctamente. Se quitaron satisfactoriamente todas las canciones de la carpeta 'Papelera'.";
						redirect($panelurl, 'playlist');
					} else {
						$_SESSION['messageautoupdate'] = "Error: La lista de reproducción ".$_POST['list']." no pudo ser actualizada. El servidor respondió '".$autoremove->response."'.";
						redirect($panelurl, 'playlist');
					}
				}
			}elseif($centovatype == "manual") {
				$centovatrack = $_POST['track'];
				$manualupdate = $server->manageSong($centovalist, $centovaaction, $centovatrack);
				if($manualupdate->status == "OK") {
					if($centovaaction == "add") {
						$_SESSION['messagemanualupdate'] = "La lista de reproducción ".$_POST['list']." ha sido actualizada correctamente. Se agregó correctamente la canción ".$_POST['track'];
						redirect($panelurl, 'playlist');
					} else {
						$_SESSION['messagemanualupdate'] = "La lista de reproducción ".$_POST['list']." ha sido actualizada correctamente. Se quitó correctamente la canción ".$_POST['track'];
						redirect($panelurl, 'playlist');
					}
				} else {
					$_SESSION['messagemanualupdate'] = "Error: La lista de reproducción ".$_POST['list']." no pudo ser actualizada. El servidor respondió '".$manualupdate->response."'.";
					redirect($panelurl, 'playlist');
				}
			} elseif($centovatype == "manage") {
				$managelist = $server->changePlaylistStatus($centovaaction, $centovalist);
				if($managelist->status == "OK") {
					if($centovaaction == "activate") {
						$_SESSION['messagestatusupdate'] = "La lista de reproducción ".$_POST['list']." ha sido activada correctamente.";
						redirect($panelurl, 'playlist');
					} else {
						$_SESSION['messagestatusupdate'] = "La lista de reproducción ".$_POST['list']." ha sido desactivada correctamente.";
						redirect($panelurl, 'playlist');
					}
				} else {
					$_SESSION['messagestatusupdate'] = "Error: La lista de reproducción ".$_POST['list']." no pudo ser modificada correctamente. El servidor respondió '".$managelist->response."'.";
					redirect($panelurl, 'playlist');
				}
			} else {
				redirect($panelurl, 'playlist');
			}
	} else {
		redirect($panelurl, '');
	}
?>