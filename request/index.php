﻿<?php @session_start();
	/*
		* Este archivo es protegido por la ley del derechos de propiedad intelectual. La copia, reproduccion o decodificacion de este codigo se prohibe estrictamente.
		* RadioRush Plugin está protegido bajo derechos de autor a nivel internacional.
		* Prohibida su modificación total o parcial así como su distribución o reproducción (utilización) de forma total o parcial con fines comerciales.
		* Para mayor información por favor ingrese al enlace de derechos de autor ubicado al pie de cualquier página.
		* Si requiere adquirir una licencia de distribución comercial o para la modificación de este software por favor contacte acualquiera de los siguientes correos electrónicos:
		* pbaltazar@app-ttitude.com
		* sieg.sb@gmail.com
		* Prohibida la realización de ingeniería inversa, crackeado, o cualquier otro método de alteración del funcionamiento normal de este software o acciones que puedan dañar la integridad del código fuente del mismo.
		* RadioRush � 2016, Todos los derechos reservados.
		* RadioRush API � 2016, Todos los derechos reservados.
		* RadioRush Plugin � 2016, Todos los derechos reservados.
		* Centova Cast�, IceCast�, Shoutcast� son marcas registradas independientes a este panel y no guardan relación comercial directa con el mismo.
	*/
	if(file_exists('config.php')) {
		include_once('config.php');
		include_once('sdk/RadioRush.php');
		include_once('request/tools.php');
		if(empty($licence) || empty($licencetype)) {
			$errormissinglicence = "Faltan datos sobre tu licencia de RadioRush en la configuración. Por favor verifica que hayas ingresado la clave de licencia de 25 carácteres y asignado el tipo de licencia que corresponde.";
		} else {
			$licencestatus = "OK";
		}
		if(empty($panelname) || empty($panelurl) || empty($companyname) || empty($companyurl) || empty($centovaurl)) {
			$errormissingfields = "Faltan variables por llenar en el archivo de configuración (config.php). Por favor edita el archivo de configuración y llena todas las variables con datos reales.";
		} else {
			$checkconfig = "OK";
		}
		$files = array('request/index.php','request/login.php','request/logout.php','request/serveraction.php','request/tools.php','request/updatelist.php','sdk/index.php','sdk/Master.php','sdk/RadioRush.php','.htaccess');
		$checkfile = "";
		for ($i = 0; $i<count($files); $i++) {
			if(file_exists($files[$i])) {
				$checkfile .= md5_file($files[$i]);
			} else {
				$checkfile .= "0";
			}
		}
		$protocols = array('http://','https://');
		$finalurl = str_replace($protocols, '', $panelurl);
		$apirevision = new RadioRush;
		$apirevision->setConnect($licence, $licencetype, 'root', 'root');
		$revisionstatus = $apirevision->checkInstall($finalurl, md5($checkfile));
		if($revisionstatus->status == 'OK') {
			$checkinstall = "OK";
		} else {
			$errorinstall = "Hemos detectado que tu instalación de RadioRush Plugin está corrompida o ha sido alterada, recuerda que este software está protegido por derechos de autor y su modificación en cualquier circunstancia está prohibida. Para solucionar este problema por favor reinstala tu copia de RadioRush Plugin.";
		}
	} else {
		$errorconfigfile = "No haz modificado el archivo de configuración del plugin de RadioRush.";
	}
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
	<meta charset="UTF-8" />
	<meta name="application-name" content="RadioRush Plugin <?php echo $pluginversion; ?>" />
	<meta name="author" content="Paris Niarfé Baltazar Salguero" />
	<meta name="description" content="<?php echo $paneldescription; ?>" />
	<meta name="keywords" content="<?php echo $panelkeywords; ?>" />
	<meta name="robots" content="index, follow, noarchive, noodp" />
	<meta name="viewport" content="width=device-width; initial-scale="1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<?php if(!isset($_SESSION['user'])) {
		echo "<title>Inicio de sesión | ".$panelname." ".$pluginversion." - ".$companyname."</title>";
	} else {
		$apirequest = new RadioRush;
		$apirequest->setConnect($licence, $licencetype, $_SESSION['user'], $_SESSION['pass']);
		if($_GET['section'] == "resumen") {
			echo "<title>Resumen | ".$panelname." ".$pluginversion." - ".$companyname."</title>";
		} elseif($_GET['section'] == "servidor") {
			echo "<title>Acciones del servidor | ".$panelname." ".$pluginversion." - ".$companyname."</title>";
		} elseif($_GET['section'] == "playlist") {
			echo "<title>Gestor de listas de reproducción | ".$panelname." ".$pluginversion." - ".$companyname."</title>";
		} else {
			echo "<title>Error 404 | ".$panelname." ".$pluginversion." - ".$companyname."</title>";
		}
	} ?>
</head>
<body>
	<header id="pageheader">
		<div id="pageheader-logo">
			<figure id="logo">
				<img src="<?php echo $panellogourl; ?>" alt="<?php echo $panelname; ?> por <?php echo $companyname; ?>" />
			</figure>
		</div>
		<div id="pageheader-title">
			<?php
				if(isset($_SESSION['user'])) {
					if($_GET['section'] == "resumen") {
						echo "<h1 id='title'>Resumen | ".$panelname." ".$pluginversion." - ".$companyname."</h1>";
					} elseif($_GET['section'] == "servidor") {
						echo "<h1 id='title'>Acciones del servidor | ".$panelname." ".$pluginversion." - ".$companyname."</h1>";
					} elseif($_GET['section'] == "playlist") {
						echo "<h1 id='title'>Gestor de listas de reproducción | ".$panelname." ".$pluginversion." - ".$companyname."</h1>";
					} else {
											echo "<h1 id='title'>Error 404 | ".$panelname." ".$pluginversion." - ".$companyname."</h1>";
					}
				} else {
					echo "<h1 id='title'>Inicio de sesión | ".$panelname." ".$pluginversion." - ".$companyname."</h1>";
				}
			?>
		</DIV>
		<nav id="menu">
			<h6 id="menu-title">Menú principal</h6>
			<?php if(isset($_SESSION['user'])) { ?>
				<a href="salir" accesskey="1">Cerrar sesión</a> | <a href="resumen" accesskey="2">Resumen</a> | <a href="servidor" accesskey="3">Acciones del servidor</a> | <a href="playlist" accesskey="4">Gestión de listas de reproducción</a> | <a href="<?php echo $companyurl; ?>" accesskey="5">Ir a <?php echo $companyname; ?></a> | <a href= "<?php echo $centovaurl; ?>" accesskey="6">Ir al panel Centova Cast de <?php echo $companyname; ?></a>
			<?php } else { ?>
				<a href="<?php echo $companyurl; ?>" accesskey="5">Ir a <?php echo $companyname; ?></a> | <a href= "<?php echo $centovaurl; ?>" accesskey="6">Ir al panel Centova Cast de <?php echo $companyname; ?></a>
			<?php } ?>
		</nav>
	</header>
	<main id="principal">
	<?php if(isset($errorconfigfile) || isset($errormissinglicence) || isset($errormissingfields) || isset($errorinstall)) {
		echo "<h2>ATENCIÓN: ".$errorconfigfile." ".$errormissinglicence." ".$errormissingfields." ".$errorinstall."</h2>";
	} else {
	}
	if(isset($_SESSION['user'])) {
		if($_GET['section'] == "resumen") { ?>
			<article id="info">
				<header>
					<h2>Resumen de cuenta para <?php echo $_SESSION['user']; ?> en <?php echo $companyname; ?></h2>
				</header>
				<section id="general_info">
					<?php
						$userconfig = $apirequest->getConfig();
						if($userconfig->status == "OK") {
							$diskusage = $apirequest->diskQuota();
							if($userconfig->response->useautodj = 1) {
								$autodjsupport = "Si";
							} else {
								$autodjsupport = "No";
							}
							echo "<dl>
								<dt>Nombre de la radio:</dt>
									<dd>".$userconfig->response->title."</dd>
								<dt>Propietario de la radio:</dt>
									<dd>".$userconfig->response->organization."</dd>
								<dt>Correo electrónico de la radio:</dt>
									<dd>".$userconfig->response->email."</dd>
								<dt>Nombre de servidor:</dt>
									<dd>".$userconfig->response->hostname."</dd>
								<dt>Dirección IP:</dt>
									<dd>".$userconfig->response->ipaddress."</dd>
								<dt>Puerto:</dt>
									<dd>".$userconfig->response->port."</dd>
								<dt>Tipo de servidor:</dt>
									<dd>".$userconfig->response->servertype."</dd>
								<dt>Soporte AutoDJ:</dt>
									<dd>".$autodjsupport."</dd>
								<dt>Software de AutoDJ:</dt>
									<dd>".$userconfig->response->apptypes."</dd>
								<dt>Radioescuchas simultáneos:</dt>
									<dd>".$userconfig->response->maxclients."</dd>
								<dt>Tráfico mensual:</dt>
									<dd>".$userconfig->response->transferlimit." MB (Sin límite si el valor es 0)</dd>
								<dt>Espacio en disco total disponible:</dt>
									<dd>".$userconfig->response->diskquota." MB (Sin límite si el valor es 0)</dd>
								<dt>Espacio en disco utilizado:</dt>
									<dd>".$diskusage->response->size." MB</dd>
								<dt>Puntos de montaje máximos:</dt>
									<dd>".$userconfig->response->mountlimit." (Sin límite si el valor es 0)</dd>
								<dt>Frecuencia de muestreo:</dt>
									<dd>".$userconfig->response->samplerate." HZ (La frecuencia real puede variar dependiendo de tu servidor)</dd>
								<dt>Canales de audio:</dt>
									<dd>".$userconfig->response->channels." CH (El valor real puede variar dependiendo de tus archivos de audio o de tu software de streaming en vivo)</dd>
								<dt>Calidad de transmición máxima:</dt>
									<dd>".$userconfig->response->maxbitrate." KBPS (La calidad real puede variar de acuerdo a tus archivos de audio o al software de streaming en vivo)</dd>
							<dl>";
						} else {
							echo "<p>Tenemos problemas al recuperar la información de tu cuenta, esto puede deberse a que la misma ha sido suspendida o a que tu servidor no está respondiendo correctamente. Intenta nuevamente más tarde. El servidor respondió: ".$userconfig->response;
						}
					?>
				</section>
			</article>
		<?php } elseif($_GET['section'] == "servidor") { ?>
			<article id="info">
				<header>
					<h2>Acciones para el servidor de <?php echo $_SESSION['user']; ?> en <?php echo $companyname; ?></h2>
				</header>
				<section id="server_actions">
					<p>Aquí puedes realizar algunas acciones dentro de tu servidor Centova Cast como apagar, encender o reiniciar el servidor o activar y desactivar el AutoDJ de tu cuenta de radio.</p>
					<form action="interactuar" method="POST">
						<fieldset>
							<legend>Acción a aplicar</legend>
							<p><label for="action">Acción a realizar:</label>
								<select id="action" name="action">
									<option value="start">Iniciar el servidor</option>
									<option value="reload">Reiniciar el servidor (Sin desconectar a los radioescuchas)</option>
									<option value="restart">Reiniciar el servidor (Desconectando a los radioescuchas y la emisión en vivo si existe)</option>
									<option value="stop">Detener el servidor</option>
									<option value="startautodj">Iniciar el AutoDJ</option>
									<option value="stopautodj">Detener el AutoDJ</option>
								</select></p>
							<p><input type="submit" id="apply" name="apply" value="Aplicar" /></p>
							<p><?php echo $_SESSION['statusaction']; ?></p>
						</fieldset>
					</form>
				</section>
			</article>
		<?php } elseif($_GET['section'] == "playlist") { ?>
			<article id="info">
				<header>
					<h2>Gestor de listas de reproducción para <?php echo $_SESSION['user']; ?> en <?php echo $companyname; ?></h2>
				</header>
				<section id="control_playlist">
					<h3>Activar y desactivar listas de reproducción</h3>
					<p>Aquí puedes administrar la activación o desactivación de tus listas de reproducción, solo debes seleccionar la lista que deseas activar o desactibar y seleccionar la acción a llevar a cabo. El desplegable indica el estado actual.</p>
					<?php
						$centovalist = $apirequest->getPlaylist();
						if($centovalist->status == "OK") {
							if($centovalist->response == false) {
								echo "<p>No tienes ninguna lista de reproducción en tu radio, por favor antes de continuar crea una lista desde el panel de control Centova Cast de ".$companyname.". Puedes acceder al mismo dando click <a href='".$centovaurl."' target='_blank'>aquí</a>.</p>";
								echo "<p>Una vez que hayas creado una o más listas de reproducción vuelve a esta página y actualiza presionando F5 o el ícono de actualización junto a la barra de direcciones de tu navegador.</p>";
							} else {?>
								<form action="actualizar/manage" method="POST">
									<fieldset>
										<legend>Control de listas de reproducción</legend>
										<p><label for="lista3">Lista de reproducción:</label>
										<select name="list" id="lista3">
											<?php for ($i = 0; $i<count($centovalist->response); $i++) {
												$list = $centovalist->response[$i];
												if($list->status == "enabled") {
													$liststatus = "Activada";
												} else {
													$liststatus = "Desactivada";
												}
												echo "<option value='".$list->title."'>".$list->title.", (".$liststatus.")</option>";
										} ?>
										</select></p>
										<p><label for="action3">Acción:</label>
										<select id="action3" name="action">
											<option value="activate">Activar</option>
											<option value="deactivate">Desactivar</option>
										</select></p>
										<p><input type="submit" value="Actualizar" id="statusupdate" name="update" /></p>
										<p><?php echo $_SESSION['messagestatusupdate']; ?></p>
									</fieldset>
								</form>
							<?php }
						} else {
							echo $centovalist->message;
						}
					?>
				</section>
				<section id="manual_update">
					<h3>Actualización Manual</h3>
					<p>A continuación selecciona la lista de reproducción y escribe el nombre de la canción que quieres agregar o quitar tal como aparece en los metadatos del archivo que cargaste a tu FTP. Es importante que dicho archivo ya se encuentre en alguna de tus carpetas FTP del AutoDJ de tu radio.</p>
					<?php
						if($centovalist->status == "OK") {
							if($centovalist->response == false) {
								echo "<p>No tienes ninguna lista de reproducción en tu radio, por favor antes de continuar crea una lista desde el panel de control Centova Cast de ".$companyname.". Puedes acceder al mismo dando click <a href='".$centovaurl."' target='_blank'>aquí</a>.</p>";
								echo "<p>Una vez que hayas creado una o más listas de reproducción vuelve a esta página y actualiza presionando F5 o el ícono de actualización junto a la barra de direcciones de tu navegador.</p>";
							} else {?>
								<form action="actualizar/manual" method="POST">
									<fieldset>
										<legend>Actualización manual de lista de reproducción</legend>
										<p><label for="lista1">Lista de reproducción:</label>
										<select name="list" id="lista1">
											<?php for ($i = 0; $i<count($centovalist->response); $i++) {
												$list = $centovalist->response[$i];
												if($list->status == "enabled") {
													$liststatus = "Activada";
												} else {
													$liststatus = "Desactivada";
												}
												echo "<option value='".$list->title."'>".$list->title.", (".$liststatus.")</option>";
											} ?>
										</select></p>
										<p><label for="action1">Acción:</label>
										<select id="action1" name="action">
											<option value="add">Agregar</option>
											<option value="remove">Quitar</option>
										</select></p>
										<p><label for="track">Nombre de la canción como aparece en los metadatos:</label>
										<input type="text" id="track" name="track" maxlength="25" placeholder="Mi canción" required /></p>
										<p><input type="submit" value="Actualizar" id="manualupdate" name="update" /></p>
										<p><?php echo $_SESSION['messagemanualupdate']; ?></p>
									</fieldset>
								</form>
							<?php }
						} else {
							echo $centovalist->message;
						}
					?>
				</section>
				<section id="auto_update">
					<h3>Actualización Automática</h3>
					<p>También podemos ofrecerte una actualización automática de listas con la cual solo debes ingresar todas las canciones que quieres agregar a una carpeta específica en tu FTP y las mismas serán agregadas a la lista de reproducción. Te explicamos como preparar tu cuenta Centova para este tipo de actualización.</p>
					<h3>Agregar canciones a lista de reproducción</h3>
					<h4>Paso 1:</h4>
					<p>Primeramente debes asegurarte que tus listas de reproducción no contengan espacios en sus nombres, pueden contener carácteres alfanuméricos pero no carácteres especiales, te damos un ejemplo de nombres de listas correctos e incorrectos:</p>
					<ol>
						<li><h5>correcto</h5></li>
							<ul>
								<li>MiListaDeReproduccion</li>
								<li>CancionesPopulares</li>
								<li>Top10Semanal</li>
							</ul>
						<li><h5>Incorrecto</h5></li>
							<ul>
								<li>Mi Lista De Reproducción</li>
								<li>Canciones populares!</li>
								<li>Top 10' semanal</li>
							</ul>
					</ol>
					<p>Como puedes notar en el primer ejemplo correcto todas las palabras están juntas y sin espacios y el caracter especial de acento no se puso, por lo que debes de asegurarte que todas tus listas tienen nombres con este formato para garantizar el correcto funcionamiento de este método de actualización automático.</p>
					<h4>Paso 2:</h4>
					<p>Ahora deberás ingresar a tu FTP y crear carpetas con el mismo nombre de tu lista de reproducción dentro de la carpeta media, aquí te damos algunos ejemplos:</p>
					<dl>
						<dt>Lista de reproducción</dt>
							<dd>Nombre de carpeta en FTP</dd>
						<dt>MiListaDeReproduccion</dt>
							<dd>MiListaDeReproduccion</dd>
						<dt>Top10Semanal</dt>
							<dd>Top10Semanal</dd>
						<dt>sanvalentin</dt>
							<dd>sanvalentin</dd>
					</dl>
					<p>Asegúrate de que el nombre de la lista y el de tu carpeta sean exactamente iguales pues de lo contrario esto no funcionará.</p>
					<h4>Paso 3:</h4>
					<p>Ahora sube a cada carpeta las canciones que quieras agregar a la lista asociada a esta carpeta, si dos o más listas comparten una canción deberás subir el archivo a ambas carpetas. También puedes crear subcarpetas dentro de estas carpetas de listas de reproducción pero no implican nada para el funcionamiento de la lista, es solo con fines de organización.</p>
					<h4>Paso 4:</h4>
					<p>Una vez agregues todas las canciones a las carpetas deseadas en el FTP, selecciona la acción "Agregar" del menú desplegable de abajo de estas instrucciones y presiona el botón "Actualizar".</p>
					<h3>Eliminar canciones de una lista de reproducción</h3>
					<p>Para eliminar canciones de una lista de reproducción debes seguir pasos similares a los descritos arriba pero con algunas diferencias importantes. Toma en cuenta que si borras un archivo MP3 de la carpeta FTP y luego actualizas con la acción "Quitar", no pasará nada a menos que cumplas con las siguientes instrucciones.</p>
					<h4>Paso 1:</h4>
					<p>Primero deberás crear una nueva carpeta en tu FTP llamada exactamente como se muestra a continuación:</p>
					<p><em>Papelera</em></p>
					<h4>Paso 2:</h4>
					<p>Ahora deberás mover todas las canciones que quieres retirar de la lista de reproducción de la carpeta de la lista a esta nueva carpeta asegurándote de no borrar o mover las canciones de esta carpeta hasta que el proceso de eliminación termine.</p>
					<p>Es importante que tomes en cuenta que este proceso debe realizarse de forma independiente para cada carpeta y que todos los pasos desde el segundo deberán de repetirse para cada lista de reproducción una vez finalizado el proceso para una lista individual y que no deberás de ejecutar esta acción en nuestro panel hasta estar seguro de los cambios.</p>
					<h4>Paso 3:</h4>
					<p>Una vez movidas todas las canciones deseadas a la "Papelera" deberás de seleccionar del menú desplegable de abajo la acción "Quitar" y presionar el botón "Actualizar".</p>
					<h4>Paso 4:</h4>
					<p>Una vez el panel te indique que el proceso ha sido exitoso podrás eleiminar permanentemente las canciones de la carpeta "Papelera", otra utilidad para esta función automática es mover un número elevado de canciones entre listas de reproducción al eliminar las canciones del FTP moviéndolas temporalmente a la carpeta "Papelera" que creamos en el FTP, usando la acción "Quitar" y luego moviendo de nuevo las canciones de la carpeta "Papelera" a la carpeta de otra lista y aplicando la acción "Agregar" sobre la nueva lista.</p>
					<p>Es importante recordar que la carpeta "Papelera" a la que se hace referencia aquí es una carpeta creada por nosotros mismos en el FTP y en ningún momento se refiere a la "Papelera de reciclaje" de tu sistema operativo.</p>
					<h3>Listas de reproducción para actualización automática</h3>
					<p>Selecciona la lista de reproducción y la acción que quieras llevar a cabo para ella.</p>
					<?php
						if($centovalist->status == "OK") {
							if($centovalist->response == false) {
								echo "<p>No tienes ninguna lista de reproducción en tu radio, por favor antes de continuar crea una lista desde el panel de control Centova Cast de ".$companyname.". Puedes acceder al mismo dando click <a href='".$centovaurl."' target='_blank'>aquí</a>.</p>";
								echo "<p>Una vez que hayas creado una o más listas de reproducción vuelve a esta página y actualiza presionando F5 o el ícono de actualización junto a la barra de direcciones de tu navegador.</p>";
							} else {?>
								<form action="actualizar/auto" method="POST">
									<fieldset>
										<legend>Actualización automática de listas de reproducción</legend>
										<p><label for="lista2">Lista de reproducción:</label>
										<select name="list" id="lista2">
											<?php for ($i = 0; $i<count($centovalist->response); $i++) {
												$list = $centovalist->response[$i];
												if($list->status == "enabled") {
													$liststatus = "Activada";
												} else {
													$liststatus = "Desactivada";
												}
												echo "<option value='".$list->title."'>".$list->title." (".$liststatus.")</option>";
											}?>
										</select></p>
										<p><label for="action2">Acción:</label>
										<select id="action2" name="action">
											<option value="add">Agregar</option>
											<option value="remove">Quitar</option>
										</select></p>
										<p><input type="submit" value="Actualizar" id="autoupdate" name="update" /></p>
										<p><?php echo $_SESSION['messageautoupdate']; ?></p>
									</fieldset>
								</form>
							<?php }
						} else {
							echo "<p>Ha ocurrido un problema y al parecer ya no tienes permisos de acceso en tu servidor, esto puede pasar porque tu cuenta ha sido eliminada, suspendida o por problemas internos con el servidor de tu provedor de streaming. Por favor cierra sesión e inténtalo más tarde.</p>";
						}
					?>
				</section>
			</article>
		<?php } else { ?>
			<article id="info">
				<header>
					<h2>Error 404 - Página no encontrada</h2>
				</header>
				<section id="notfound">
					<?php echo $error404; ?>
				</section>
			</article>
		<?php }
	} else { ?>
		<article id="info">
			<header>
				<h2>Iniciar sesión en <?php echo $panelname; ?> de <?php echo $companyname; ?></h2>
			</header>
			<section id="loginpage">
				<form action="autenticacion" method="POST">
					<fieldset>
						<legend>Inicio de sesión</legend>
						<p><label for="user">Nombre de usuario (Max 20 carácteres):</label>
						<?php if(isset($licencestatus) AND isset($checkconfig) AND isset($checkinstall)) { ?>
							<input type="text" id="user" name="user" maxlength="20" placeholder="Tu usuario" required autofocus /></p>
						<?php } else { ?>
							<input type="text" id="user" name="user" maxlength="20" placeholder="Tu usuario" required autofocus readonly /></p>
						<?php } ?>
						<p><label for="pass">Contraseña (Max 32 carácteres):</label>
						<?php if(isset($licencestatus) AND isset($checkconfig) AND isset($checkinstall)) { ?>
							<input type="password" id="pass" name="pass" maxlength="32" required /></p>
						<?php } else { ?>
							<input type="password" id="pass" name="pass" maxlength="32" required readonly /></p>
						<?php } ?>
						<p><input type="submit" value="Ingresar" id="login" name="login" /></p>
						<p><?php echo $_SESSION['errorlogin']; ?></p>
					</fieldset>
				</form>
			</section>
		</article>
	<?php } ?>
	</main>
	<footer id="footerpage">
		<?php if($licencetype == "branded") { ?>
			<p><a href="<?php echo $companyurl; ?>"><?php echo $companyname; ?> &copy 2016</a>, Derechos de uso autorizados por el creador | <a href="http://www.safecreative.org/work/1603176933104-radiorush" target="_blank"><span>Powered by RadioRush</span> <span>&copy 2016 </span><span>Paris Niarfé Baltazar Salguero | App-ttitude.com. Todos los derechos reservados.</span></a></p>
		<?php } else { ?>
			<p><a href="<?php echo $companyurl; ?>"><?php echo $companyname; ?> &copy 2016</a>, Derechos de uso autorizados por el creador</p>
		<?php } ?>
	</footer>
</body>
</html>
<?php
	$_SESSION['errorlogin'] = "";
	$_SESSION['messagestatusupdate'] = "";
	$_SESSION['messagemanualupdate'] = "";
	$_SESSION['messageautoupdate'] = "";
	$_SESSION['statusaction'] = "";
?>