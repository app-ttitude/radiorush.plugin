﻿<?php @session_start();
	/*
		* Este archivo es protegido por la ley del derechos de propiedad intelectual. La copia, reproduccion o decodificacion de este codigo se prohibe estrictamente.
		* RadioRush Plugin está protegido bajo derechos de autor a nivel internacional.
		* Prohibida su modificación total o parcial así como su distribución o reproducción (utilización) de forma total o parcial con fines comerciales.
		* Para mayor información por favor ingrese al enlace de derechos de autor ubicado al pie de cualquier página.
		* Si requiere adquirir una licencia de distribución comercial o para la modificación de este software por favor contacte acualquiera de los siguientes correos electrónicos:
		* pbaltazar@app-ttitude.com
		* sieg.sb@gmail.com
		* Prohibida la realización de ingeniería inversa, crackeado, o cualquier otro método de alteración del funcionamiento normal de este software o acciones que puedan dañar la integridad del código fuente del mismo.
		* RadioRush � 2016, Todos los derechos reservados.
		* RadioRush API � 2016, Todos los derechos reservados.
		* RadioRush Plugin � 2016, Todos los derechos reservados.
		* Centova Cast�, IceCast�, Shoutcast� son marcas registradas independientes a este panel y no guardan relación comercial directa con el mismo.
	*/
	include_once("../config.php");
	include_once("../sdk/RadioRush.php");
	include_once("tools.php");
	$_SESSION['errorlogin'] = "";
	if(isset($_POST['login'])) {
		$centovauser = $_POST['user'];
		$centovapass = $_POST['pass'];
			$login = new RadioRush;
			$login->setConnect($licence, $licencetype, $centovauser, $centovapass);
			$loginresponse = $login->getLogin();
			if($loginresponse->status == "OK") {
				$_SESSION['user'] = $centovauser;
				$_SESSION['pass'] = $centovapass;
				redirect($panelurl, 'resumen');
			} else {
				$_SESSION['errorlogin'] = "Error: No se pudo comunicar con el servidor Centova Cast o tu usuario y contraseña son incorrectos, verifica tu información e inténtalo nuevamente. El servidor respondió:'".$loginresponse->response."'";
				redirect($panelurl, '');
			}
	} else {
		redirect($panelurl, '');
	}
?>