﻿<?php @session_start();
	/*
		* Este archivo es protegido por la ley del derechos de propiedad intelectual. La copia, reproduccion o decodificacion de este codigo se prohibe estrictamente.
		* RadioRush Plugin está protegido bajo derechos de autor a nivel internacional.
		* Prohibida su modificación total o parcial así como su distribución o reproducción (utilización) de forma total o parcial con fines comerciales.
		* Para mayor información por favor ingrese al enlace de derechos de autor ubicado al pie de cualquier página.
		* Si requiere adquirir una licencia de distribución comercial o para la modificación de este software por favor contacte acualquiera de los siguientes correos electrónicos:
		* pbaltazar@app-ttitude.com
		* sieg.sb@gmail.com
		* Prohibida la realización de ingeniería inversa, crackeado, o cualquier otro método de alteración del funcionamiento normal de este software o acciones que puedan dañar la integridad del código fuente del mismo.
		* RadioRush � 2016, Todos los derechos reservados.
		* RadioRush API � 2016, Todos los derechos reservados.
		* RadioRush Plugin � 2016, Todos los derechos reservados.
		* Centova Cast�, IceCast�, Shoutcast� son marcas registradas independientes a este panel y no guardan relación comercial directa con el mismo.
	*/
	include_once("../config.php");
	include_once("../sdk/RadioRush.php");
	include_once("tools.php");
	$_SESSION['statusaction'] = "";
	if(isset($_POST['apply'])) {
		$centovauser = $_SESSION['user'];
		$centovapass = $_SESSION['pass'];
		$server = new RadioRush;
		$server->setConnect($licence, $licencetype, $centovauser, $centovapass);
		if($_POST['action'] == "start") {
			$startserver = $server->startServer();
			if($startserver->status == "OK") {
				$_SESSION['statusaction'] = "El servidor fué iniciado correctamente. El servidor respondió '".$startserver->response."'.";
				redirect($panelurl, 'servidor');
			} else {
				$_SESSION['statusaction'] = "Error: El servidor no pudo ser iniciado correctamente. El servidor respondió '".$startserver->response."'.";
				redirect($panelurl, 'servidor');
			}
		} elseif($_POST['action'] == "reload") {
			$reloadserver = $server->reloadServer();
			if($reloadserver->status == "OK") {
				$_SESSION['statusaction'] = "El servidor fué reiniciado correctamente sin desconectar a los radioescuchas. El servidor respondió '".$reloadserver->response."'.";
				redirect($panelurl, 'servidor');
			} else {
				$_SESSION['statusaction'] = "Error: El servidor no pudo ser reiniciado correctamente. El servidor respondió '".$reloadserver->response."'.";
				redirect($panelurl, 'servidor');
			}
		} elseif($_POST['action'] == "restart") {
			$restartserver = $server->restartServer();
			if($restartserver->status == "OK") {
				$_SESSION['statusaction'] = "El servidor fué reiniciado correctamente desconectando a todos los radioescuchas. El servidor respondió '".$restartserver->response."'.";
				redirect($panelurl, 'servidor');
			} else {
				$_SESSION['statusaction'] = "Error: El servidor no pudo ser reiniciado correctamente. El servidor respondió '".$restartserver->response."'.";
				redirect($panelurl, 'servidor');
			}
		} elseif($_POST['action'] == "stop") {
			$stopserver = $server->stopServer();
			if($stopserver->status == "OK") {
				$_SESSION['statusaction'] = "El servidor fué detenido correctamente. El servidor respondió '".$stopserver->response."'.";
				redirect($panelurl, 'servidor');
			} else {
				$_SESSION['statusaction'] = "Error: El servidor no pudo ser detenido correctamente. El servidor respondió '".$stopserver->response."'.";
				redirect($panelurl, 'servidor');
			}
		} elseif($_POST['action'] == "startautodj") {
			$startautodj = $server->manageAutoDj("up");
			if($startautodj->status == "OK") {
				$_SESSION['statusaction'] = "El AutoDJ fué iniciado correctamente. El servidor respondió '".$startautodj->response."'.";
				redirect($panelurl, 'servidor');
			} else {
				$_SESSION['statusaction'] = "Error: El AutoDJ no pudo ser iniciado correctamente. El servidor respondió '".$startautodj->response."'.";
				redirect($panelurl, 'servidor');
			}
		} elseif($_POST['action'] == "stopautodj") {
			$stopautodj = $server->manageAutoDj("down");
			if($stopautodj->status == "OK") {
				$_SESSION['statusaction'] = "El AutoDJ fué detenido correctamente. El servidor respondió '".$stopautodj->response."'.";
				redirect($panelurl, 'servidor');
			} else {
				$_SESSION['statusaction'] = "Error: El AutoDJ no pudo ser detenido correctamente. El servidor respondió '".$stopautodj->response."'.";
				redirect($panelurl, 'servidor');
			}
		} else {
			$_SESSION['statusaction'] = "Lo sentimos pero el servidor reportó un error inesperado y no identificado. Por favor inténtalo de nuevo más tarde.";
			redirect($panelurl, 'servidor');
		}
	} else {
		redirect($panelurl, '');
	}
?>