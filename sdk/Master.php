﻿<?php
	/*
		* Este archivo es protegido por la ley del derechos de propiedad intelectual. La copia, reproduccion o decodificacion de este codigo se prohibe estrictamente.
		* RadioRush Plugin está protegido bajo derechos de autor a nivel internacional.
		* Prohibida su modificación total o parcial así como su distribución o reproducción (utilización) de forma total o parcial con fines comerciales.
		* Para mayor información por favor ingrese al enlace de derechos de autor ubicado al pie de cualquier página.
		* Si requiere adquirir una licencia de distribución comercial o para la modificación de este software por favor contacte acualquiera de los siguientes correos electrónicos:
		* pbaltazar@app-ttitude.com
		* sieg.sb@gmail.com
		* Prohibida la realización de ingeniería inversa, crackeado, o cualquier otro método de alteración del funcionamiento normal de este software o acciones que puedan dañar la integridad del código fuente del mismo.
		* RadioRush � 2016, Todos los derechos reservados.
		* RadioRush API � 2016, Todos los derechos reservados.
		* RadioRush Plugin � 2016, Todos los derechos reservados.
		* Centova Cast�, IceCast�, Shoutcast� son marcas registradas independientes a este panel y no guardan relación comercial directa con el mismo.
	*/

	class Master {
		protected $options = array();
		protected $query;
		protected $data;

		public function __construct($options = array()) {
			try {
				if(empty($options)) {
					$errormessage = "No se resivieron los parámetros de la consulta correctamente.";
					echo $errormessage;
				} else {
					$this->options = $options;
					$this->build_query();
					$this->request();
				}
			} catch(Exception $e) {
				echo $e->getMessage();
			}
		}

		public function get() {
			return json_decode($this->data);
		}

		private function build_query() {
			$this->query = 'http://apis.app-ttitude.com/radiorush/api.php?'.http_build_query($this->options);
		}

		private function request() {
			try {
				$this->data = $this->curl($this->query);
			} catch(Exception $e) {
				echo $e->getMessage();
			}
		}

		private function curl($url) {
			if(function_exists('curl_init')) {
				$consult = curl_init();

				curl_setopt($consult, CURLOPT_HEADER, 0);
				curl_setopt($consult, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($consult, CURLOPT_CONNECTTIMEOUT, 5);
				curl_setopt($consult, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)');
				curl_setopt($consult, CURLOPT_URL, $url);
				curl_setopt($consult, CURLOPT_SSLVERSION, 3);
				curl_setopt($consult, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($consult, CURLOPT_SSL_VERIFYHOST, 2);
				$return = curl_exec($consult);
				curl_close($consult);

				return $return;
			} else {
				return file_get_contents($url);
			}
		}

	}
?>