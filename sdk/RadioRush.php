﻿<?php
	/*
		* Este archivo es protegido por la ley del derechos de propiedad intelectual. La copia, reproduccion o decodificacion de este codigo se prohibe estrictamente.
		* RadioRush Plugin está protegido bajo derechos de autor a nivel internacional.
		* Prohibida su modificación total o parcial así como su distribución o reproducción (utilización) de forma total o parcial con fines comerciales.
		* Para mayor información por favor ingrese al enlace de derechos de autor ubicado al pie de cualquier página.
		* Si requiere adquirir una licencia de distribución comercial o para la modificación de este software por favor contacte acualquiera de los siguientes correos electrónicos:
		* pbaltazar@app-ttitude.com
		* sieg.sb@gmail.com
		* Prohibida la realización de ingeniería inversa, crackeado, o cualquier otro método de alteración del funcionamiento normal de este software o acciones que puedan dañar la integridad del código fuente del mismo.
		* RadioRush � 2016, Todos los derechos reservados.
		* RadioRush API � 2016, Todos los derechos reservados.
		* RadioRush Plugin � 2016, Todos los derechos reservados.
		* Centova Cast�, IceCast�, Shoutcast� son marcas registradas independientes a este panel y no guardan relación comercial directa con el mismo.
	*/

	require_once('Master.php');

	class RadioRush {
		protected $radiorush = array(
			'licence' => '',
			'apipass' => '',
			'user' => '',
			'pass' => '');

		public function setConnect($licence, $licencetype, $user, $pass) {
			if($licencetype == 'branded' || $licencetype == 'nobranded') {
				$apipass = hash_hmac_file('md5', 'index.php', $licencetype);
			} else {
				$apipass = $licencetype;
			}
			$this->radiorush = array(
				'licence' => $licence,
				'apipass' => $apipass,
				'user' => $user,
				'pass' => $pass);
			return $this;
		}

		public function checkInstall($installurl, $installrevision) {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[install]' => $installurl,
				'a[revision]' => $installrevision,
				'a[action]' => 'checkinstall');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function getLogin() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'login');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function getPlaylist() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'playlist');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function manageSong($playlist, $action, $track) {
			$playlist = $playlist;
			$action = $action;
			$track = $track;
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => $action,
				'a[playlist]' => $playlist,
				'a[track]' => $track);
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function autoAdd($playlist) {
			$playlist = $playlist;
			$options = array(
				'licence' => $this-radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'autoadd',
				'a[playlist]' => $playlist);
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function autoRemove($playlist) {
			$playlist = $playlist;
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'autoremove',
				'a[playlist]' => $playlist);
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function getConfig() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'showconfig');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function getInfo() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'getinfo');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function startServer() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'start');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function reloadServer() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'reload');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function restartServer() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'restart');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function stopServer() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'stop');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function manageAutoDj($state) {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'autodj',
				'a[turn]' => $state);
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function diskQuota() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'getdisk');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function infoStream() {
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'infostream');
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

		public function changePlaylistStatus($change, $playlist) {
			$playlist = $playlist;
			$change = $change;
			$options = array(
				'licence' => $this->radiorush['licence'],
				'apipass' => $this->radiorush['apipass'],
				'a[user]' => $this->radiorush['user'],
				'a[pass]' => $this->radiorush['pass'],
				'a[action]' => 'changeplayliststatus',
				'a[playlist]' => $playlist,
				'a[change]' => $change);
			$radiorushrequest = new Master($options);
			$radiorushresponse = $radiorushrequest->get();

			return $radiorushresponse;
		}

	}
?>